# Truck Sim Plugin

Holds the plugin file that has to be used with Euro Truck Simulator 2 or American Truck Simulator to get the telemetry data of the game.

You can download the file with this link: https://gitlab.com/virtual-company/truck-sim-plugin/raw/master/scs-telemetry.dll?inline=false

After download you have to place the file inside of a plugin folder next to the .exe for example:

C:\Program Files (x86)\SteamLibrary\steamapps\common\Euro Truck Simulator 2\bin\win_x64\plugins\scs-telemetry.dll

**The folder "plugins" might not exist there so you have to create it by yourself**